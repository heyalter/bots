package bots

import (
	"os"
	"net/url"
	"time"

	"github.com/go-ini/ini"
	"github.com/ifo/gozulipbot"
)


func loadConfig(filename string, config ZuliprcProvider) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}

	cfg, err := ini.Load(file)
	if err != nil {
		return err
	}

	return cfg.StrictMapTo(config)
}

func InitZulip(config ZuliprcProvider) (*gozulipbot.Bot, error) {
	filename := "zuliprc"
	if len(os.Args) > 1 {
		filename = os.Args[1]
	}
	if err := loadConfig(filename, config); err != nil {
		return nil, err
	}

	apiurl, err := url.JoinPath(config.GetZulipSiteURL(), "/api/v1/")
	if err != nil {
		return nil, err
	}

	var bot = &gozulipbot.Bot{
		Email:   config.GetBotEmail(),
		APIKey:  config.GetAuthKey(),
		APIURL:  apiurl,
		Backoff: 1 * time.Second,
	}
	bot.Init()
	return bot, nil
}
