package bots

type ZuliprcProvider interface {
	GetBotEmail() string
	GetAuthKey() string
	GetZulipSiteURL() string
}


type ZuliprcAPISection struct {
	Email string `ini:"email"`
	Key   string `ini:"key"`
	Site  string `ini:"site"`
}

func (zas ZuliprcAPISection) GetBotEmail() string {
	return zas.Email
}

func (zas ZuliprcAPISection) GetAuthKey() string {
	return zas.Key
}

func (zas ZuliprcAPISection) GetZulipSiteURL() string {
	return zas.Site
}

var _ ZuliprcProvider = ZuliprcAPISection{}

